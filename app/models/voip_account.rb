class VoipAccount < ActiveRecord::Base
	belongs_to :owner, :class_name => "User"

	def cdr_data(hash)
		start_date = hash[:from]
		end_date = hash[:to]

		if ENV["RAILS_ENV"] == "development"
			table_name = "asterisk.cdr"
		else
			table_name = "asterisk_cdr.cdr"
		end
		
		sql = "SELECT billsec, calldate, dstphone, amaflags, disposition FROM #{table_name} WHERE calldate > '#{start_date} 00:01' AND calldate < '#{end_date} 23:59' AND accountcode = '#{name}' ORDER BY calldate"
		
		data = Array.new

		total = 0
		print("SQL: #{sql}")
		connection.execute(sql).each do |row|
			if row[3].to_i == 2 and row[4] == 'ANSWERED'
				cost = VoipCost.from_date(row[1]).matching(row[2]).first
				if cost != nil
					price = cost.for_duration(row[0].to_i)
					total += price
					data << {:duration => row[0], :date => row[1].to_i, :number => row[2], :price => price, :cost => cost.name, :cost_id => cost.id}
				else
					data << {:duration => row[0], :date => row[1].to_i, :number => row[2], :price => "UNKNOWN"}
				end
			end
		end
		
		{:total => total, :data => data}
	end
end
