class Mailuser < ActiveRecord::Base
	belongs_to :domain

	def setpassword(pw)
		self.password = Digest::MD5.hexdigest(pw)
	end

	def password_is?(pw)
		Digest::MD5.hexdigest(pw) == password
	end

	def valid_password(pw)
		Digest::MD5.hexdigest(pw) == self.password
	end
end
