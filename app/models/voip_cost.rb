class VoipCost < ActiveRecord::Base
	def for_duration(seconds)
		(flagfall.to_f + timecost * (seconds.to_f / timeperiod.to_f)).ceil
	end

	def self.matching(number)
		where(["? LIKE number", number]).order("CHAR_LENGTH(number) desc")
	end
	
	def self.from_date(date)
		where(["start_date < ?", date]).order("start_date desc")
	end

end
