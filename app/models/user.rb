class User < ActiveRecord::Base
	has_many :domains, :foreign_key => "owner_id"
	has_many :voip_accounts, :foreign_key => "owner_id"

	def setpassword(pw)
		self.password = Digest::MD5.hexdigest(pw)
	end

	def password_is?(pw)
		Digest::MD5.hexdigest(pw) == password
	end
	
# if this returns nil, you haven't authenticated.
# Otherwise it'll return the auth'ed user
	def User.authenticate(login, password, options = { })
		user = User.find(:first,
			:conditions => [ "login = ?", login.strip] )
		if user == nil
			return nil
		end

		if options[:pwdhashed] == true
			if user.password == password
				return user
			else
				return nil
			end
		else
			if user.password_is?(password)
				return user
			else
				return nil
			end
		end
	end
end
