class Domain < ActiveRecord::Base
	belongs_to :owner, :class_name => "User"
	has_many :aliases, :class_name => "MailaliasSource"
	has_many :users, :class_name => "Mailuser"
	has_many :mailalias_source, :class_name => "MailaliasSource"
	has_many :mailuser, :class_name => "Mailuser"
	
	def expired
		return DateTime.now >= paiduntil
	end
end
