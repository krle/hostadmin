class MailaliasSource < ActiveRecord::Base
	belongs_to :domain
	has_many :mailalias_destinations

	def destinations(include_new = true)
		if include_new
			mailalias_destinations.all + [MailaliasDestination.new]
		else
			mailalias_destinations
		end
	end

	def destinations_attributes=(attributes)
		attributes.each do |key, value|
			if value["_destroy"].to_i == 1
				temp = mailalias_destinations.find(value[:id].to_i)
				temp.destroy
			elsif valid_destination(value)
				if value.include?(:id)
					temp = mailalias_destinations.find(value[:id].to_i)
					temp.destination = value[:destination].strip
					temp.save
				else
					temp = MailaliasDestination.new
					temp.destination = value[:destination].strip
					temp.source = self
					temp.save
				end
			end
		end
	end

private
	def valid_destination(ahash)
		split = ahash[:destination].split("@")
		if split.length != 2
			return false
		end

		if split[1].length < 4
			return false
		end

		true
	end

end
