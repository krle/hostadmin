class DomainsController < ApplicationController
	before_filter :logged_in
	
  # GET /domains
  # GET /domains.xml
  def index
		if @currentuser.administrator
    	@domains = Domain.find(:all)
		else
			@domains = Domain.find_all_by_owner_id(@currentuser.id)
		end

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @domains }
    end
		if false
			redirect_to :controller => 'index', :action => 'login'			
		end

  end

  # GET /domains/1
  # GET /domains/1.xml
  def show
    @domain = Domain.find(params[:id])
		if !@currentuser.administrator and @domain.owner != @currentuser
	    respond_to do |format|
	      format.html { redirect_to :action => "index" }
	    end
			return
		end

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @domain }
    end
  end

  # GET /domains/new
  # GET /domains/new.xml
  def new
    @domain = Domain.new
		if !@currentuser.administrator
			redirect_to :action => "index"
			return
		end
		
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @domain }
    end
  end

  # GET /domains/1/edit
  def edit
    @domain = Domain.find(params[:id])
		if !@currentuser.administrator
			redirect_to :action => "index"
			return
		end
  end

  # POST /domains
  # POST /domains.xml
  def create
    @domain = Domain.new(params[:domain])
		if !@currentuser.administrator
			redirect_to :action => "index"
			return
		end
		
    respond_to do |format|
      if @domain.save
        flash[:notice] = 'Domain was successfully created.'
        format.html { redirect_to(@domain) }
        format.xml  { render :xml => @domain, :status => :created, :location => @domain }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @domain.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /domains/1
  # PUT /domains/1.xml
  def update
    @domain = Domain.find(params[:id])
		if !@currentuser.administrator
			redirect_to :action => "index"
			return
		end
    respond_to do |format|
      if @domain.update_attributes(params[:domain])
        flash[:notice] = 'Domain was successfully updated.'
        format.html { redirect_to(@domain) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @domain.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /domains/1
  # DELETE /domains/1.xml
  def destroy
    @domain = Domain.find(params[:id])
		if !@currentuser.administrator
			redirect_to :action => "index"
			return
		end

		@domain.destroy
    respond_to do |format|
      format.html { redirect_to(domains_url) }
      format.xml  { head :ok }
    end
  end
end
