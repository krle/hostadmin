class IndexController < ApplicationController
	before_filter :logged_in, :except => ["login", "change_email_password"]

	def index
	end
	
	def login
		if params[:user] != nil
			user = User.authenticate(params[:user][:login],
				params[:user][:password])
			if user != nil 
				session["login"] = user.login
				session["password"] = user.password
				if @origin != nil
					redirect_to :action => @origin[:action],
						:controller => @origin[:controller], :id => @origin[:id]
					return
				else
					redirect_to :action => "index", :controller => "domains"
				end
			end
		end
	end

	def change_email_password
		if params[:email_user] != nil
			query = params[:email_user]
			@username = query[:email]
			splitname = @username.split("@")
			domain = Domain.where(:name => splitname[1]).first
			if domain == nil
				@errors = "Domain not found"
				return
			end

			if !verify_recaptcha
				@errors = "Unable to verify Captcha"
				return
			end

			user = domain.mailuser.where(:name => splitname[0]).first
			if user == nil
				@errors = "Username or Password invalid"
				return
			end

			if !user.valid_password(query[:password].strip)
				@errors = "Username or Password invalid"
				return
			end

			if query[:new_password_1].strip != query[:new_password_2].strip
				@errors = "New passwords don't match"
				return
			end

			user.setpassword(query[:new_password_1].strip)
			user.save

			@errors = "Password Changed!"

		else
			@username = ""
		end
#"email_user"=>{"email"=>"test", "password"=>"[FILTERED]", "new_password_1"=>"[FILTERED]", "new_password_2"=>"[FILTERED]"}
	end
	
	def logout
		session["login"] = nil
		session["password"] = nil
	end
end
