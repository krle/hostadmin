class MailaliasDestinationsController < ApplicationController
		before_filter :logged_in

		def allowed_to_edit(mailalias_destination)
			if mailalias_destination.source.domain.owner != @currentuser and !@currentuser.administrator
				redirect_to(:action => "index", :controller => "domains")
			end
		end

	  def show
	    @mailalias_destination = MailaliasDestination.find(params[:id])
		allowed_to_edit(@mailalias_destination)

	    respond_to do |format|
	      format.html # this never really happens...
	      format.xml  { render :xml => @mailalias_source }
	    end
	  end

	  def new
	    @mailalias_destination = MailaliasDestination.new
		@mailalias_source = MailaliasSource.find(params[:mailalias_source_id].to_i)
		@mailalias_destination.source = @mailalias_source	

	    respond_to do |format|
	      format.html # this never really happens...
	      format.xml  { render :xml => @mailuser }
	    end
	  end

	  def edit
	    @mailalias_destination = MailaliasDestination.find(params[:id])
		allowed_to_edit(@mailalias_destination)
	  end

	  def create
	    @mailalias_destination = MailaliasDestination.new(params[:mailalias_destination])
		@mailalias_source = MailaliasSource.find(params[:mailalias_source_id].to_i)
		@mailalias_destination.source = @mailalias_source
		allowed_to_edit(@mailalias_destination)
		saved = @mailalias_destination.save

	    respond_to do |format|
	      if saved
	        flash[:notice] = 'Mailalias Destination was successfully created.'
	        format.html { redirect_to(edit_mailalias_source_path(@mailalias_source)) }
	        format.xml  { render :xml => @mailalias_source, :status => :created, :location => @mailuser }
	      else
	        flash[:notice] = 'Unable to create destination.'
	        format.html { render :action => "new" }
	        format.xml  { render :xml => @mailalias_source.errors, :status => :unprocessable_entity }
	      end
	    end
	  end

	  def update
	    @mailalias_destination = MailaliasDestination.find(params[:id])
		allowed_to_edit(@mailalias_destination)
		saved = @mailalias_destination.update_attributes(params[:mailalias_destination])

	    respond_to do |format|
	      if saved
	        flash[:notice] = 'Mailalias Destination was successfully Updated.'
	        format.html { redirect_to(edit_mailalias_source_path(@mailalias_destination.source)) }
	        format.xml  { head :ok }
	      else
	        flash[:notice] = 'Unable to save destination.'
	        format.html { render :action => "edit" }
	        format.xml  { render :xml => @mailalias_source.errors, :status => :unprocessable_entity }
	      end
	    end
	  end

	  def destroy
	    @mailalias_destination = MailaliasDestination.find(params[:id])
		allowed_to_edit(@mailalias_destination)
		@mailalias_destination.destroy()

	    respond_to do |format|
				format.html { redirect_to(edit_mailalias_source_path(@mailalias_destination.source)) }
	      format.xml  { head :ok }
	    end
	  end
	end