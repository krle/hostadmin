class VoipAccountsController < ApplicationController
	before_filter :logged_in
	
	# GET /voip_accounts
	# GET /voip_accounts.json
	def index
		if @currentuser.administrator
			@voip_accounts = VoipAccount.all
		else
			@voip_accounts = VoipAccount.where(:owner_id => @currentuser.id)
		end

		respond_to do |format|
			format.html # index.html.erb
			format.json { render json: @voip_accounts }
		end
	end

	# GET /voip_accounts/1
	# GET /voip_accounts/1.json
	def show
		@voip_account = VoipAccount.find(params[:id])
		if !@currentuser.administrator and @voip_account.owner != @currentuser
			redirect_to :action => "index"
			return
		end

		respond_to do |format|
			format.html # show.html.erb
			format.json { render json: @voip_account }
		end
	end

	def cdr
		@voip_account = VoipAccount.find(params[:id])
		if !@currentuser.administrator and @voip_account.owner != @currentuser
			redirect_to :action => "index"
			return
		end

		@date2 = Date.today
		@date1 = @date2.at_beginning_of_month 


		respond_to do |format|
			format.html # show.html.erb
			#format.json { render json: @voip_account }
		end
	end

	def cdr_data
		@voip_account = VoipAccount.find(params[:id])
		if !@currentuser.administrator and @voip_account.owner != @currentuser
			redirect_to :action => "index"
			return
		end

		date1 = Date.civil(params[:date1][:year].to_i, params[:date1][:month].to_i, params[:date1][:day].to_i)
		date2 = Date.civil(params[:date2][:year].to_i, params[:date2][:month].to_i, params[:date2][:day].to_i)

		@cdrs = @voip_account.cdr_data({:from => date1, :to => date2})

		respond_to do |format|
			format.json { 
				render json: {
					from: date1, to: date2, voip_account_id: @voip_account.id,
					cdrs: @cdrs[:data], total: @cdrs[:total]
				}
			}
			format.csv {
				require 'csv'
			}
		end
	end

	# GET /voip_accounts/new
	# GET /voip_accounts/new.json
	def new
		if !@currentuser.administrator
			redirect_to :action => "index"
			return
		end
		@voip_account = VoipAccount.new

		respond_to do |format|
			format.html # new.html.erb
			format.json { render json: @voip_account }
		end
	end

	# GET /voip_accounts/1/edit
	def edit
		@voip_account = VoipAccount.find(params[:id])
		if !@currentuser.administrator and @voip_account.owner != @currentuser
			redirect_to :action => "index"
			return
		end
	end

	# POST /voip_accounts
	# POST /voip_accounts.json
	def create
		if !@currentuser.administrator
			redirect_to :action => "index"
			return
		end		
		@voip_account = VoipAccount.new(params[:voip_account])

		respond_to do |format|
			if @voip_account.save
				format.html { redirect_to @voip_account, notice: 'Voip account was successfully created.' }
				format.json { render json: @voip_account, status: :created, location: @voip_account }
			else
				format.html { render action: "new" }
				format.json { render json: @voip_account.errors, status: :unprocessable_entity }
			end
		end
	end

	# PUT /voip_accounts/1
	# PUT /voip_accounts/1.json
	def update
		@voip_account = VoipAccount.find(params[:id])
		if !@currentuser.administrator and @voip_account.owner != @currentuser
			redirect_to :action => "index"
			return
		end

		respond_to do |format|
			if @voip_account.update_attributes(params[:voip_account])
				format.html { redirect_to @voip_account, notice: 'Voip account was successfully updated.' }
				format.json { head :ok }
			else
				format.html { render action: "edit" }
				format.json { render json: @voip_account.errors, status: :unprocessable_entity }
			end
		end
	end

	# DELETE /voip_accounts/1
	# DELETE /voip_accounts/1.json
	def destroy
		@voip_account = VoipAccount.find(params[:id])
		if !@currentuser.administrator and @voip_account.owner != @currentuser
			redirect_to :action => "index"
			return
		end

		@voip_account.destroy

		respond_to do |format|
			format.html { redirect_to voip_accounts_url }
			format.json { head :ok }
		end
	end
end
