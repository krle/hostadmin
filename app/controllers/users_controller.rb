class UsersController < ApplicationController
	before_filter :logged_in
  # GET /users
  # GET /users.xml
  def index
		if @currentuser.administrator
    	@users = User.find(:all)
		else
			@users = [ @currentuser ]
		end

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @users }
    end
  end

  # GET /users/1
  # GET /users/1.xml
  def show
		@user = User.find(params[:id])
		if !@currentuser.administrator and @user.id != @currentuser.id
    	@user = nil
		end
		
    respond_to do |format|
      format.html {
				if @user == nil
					redirect_to :action => "index"
				end
			}
      format.xml  {
				render :xml => @user
			}
    end
  end

  # GET /users/new
  # GET /users/new.xml
  def new
		if @currentuser.administrator
			@user = User.new
		else
			@user = nil
		end
    respond_to do |format|
      format.html {
				if @user == nil
					redirect_to :action => "index"
				end
			}
      format.xml {
	 			render :xml => @user
			}
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
		if @user.id != @currentuser.id and !@currentuser.administrator
			redirect_to(:action => "index")
		end
		if @user == @currentuser
			@title = "Modify Profile"
		else
			@title = "Edit User"
		end
  end

  # POST /users
  # POST /users.xml
  def create
		if @currentuser.administrator
			password1 = params[:user][:password1]
			params[:user].delete(:password1)
			password2 = params[:user][:password2]
			params[:user].delete(:password2)
			@user = User.new(params[:user])
			if password1 != ""
				if password1 == password2
					@user.setpassword(password1)
					saved = @user.save
				else
					flash[:notice] = "Passwords were not the same"
				end
			else
				saved = @user.save
			end
		else
				@user = nil
		end
    respond_to do |format|
      if saved
        flash[:notice] = 'User was successfully created.'
        format.html { redirect_to(@user) }
        format.xml  { render :xml => @user, :status => :created, :location => @user }
      elsif @user == nil
			# NOT ALLOWED
			else
        format.html { render :action => "new" }
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.xml
  def update
    @user = User.find(params[:id])
		if @user.id != @currentuser.id and !@currentuser.administrator
			@user = nil
		else
			password1 = params[:user][:password1]
			params[:user].delete(:password1)
			password2 = params[:user][:password2]
			params[:user].delete(:password2)
			if password1 != ""
				if password1 == password2
					@user.setpassword(password1)
				else
					flash[:notice] = "Passwords were not the same"
				end
			end
			if @currentuser.administrator
				updated = @user.update_attributes(params[:user])
			else
				updated = @user.save
			end
		end
    respond_to do |format|
      if updated
        flash[:notice] = 'User was successfully updated.'
        format.html { redirect_to(@user) }
        format.xml  { head :ok }
      elsif @user == nil
			# NOT ALLOWED
			else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.xml
  def destroy
    @user = User.find(params[:id])
		if @user.id != @currentuser.id and @currentuser.administrator
			@user.destroy
		end

    respond_to do |format|
      format.html { redirect_to(users_url) }
      format.xml  { head :ok }
    end
  end
end
