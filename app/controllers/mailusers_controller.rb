class MailusersController < ApplicationController
	before_filter :logged_in

	def allowed_to_edit(mailuser)
		if mailuser.domain.owner != @currentuser and !@currentuser.administrator
			redirect_to(:action => "index", :controller => "email")
		end
	end

  # GET /mailusers/1
  # GET /mailusers/1.xml
  def show
    @mailuser = Mailuser.find(params[:id])
		allowed_to_edit(@mailuser)
		@domain = @mailuser.domain

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @mailuser }
    end
  end

  # GET /mailusers/new
  # GET /mailusers/new.xml
  def new
    @mailuser = Mailuser.new
		@domain = Domain.find(params[:domain_id])
    @mailuser.domain_id = @domain.id

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @mailuser }
    end
  end

  # GET /mailusers/1/edit
  def edit
    @mailuser = Mailuser.find(params[:id])
		allowed_to_edit(@mailuser)
		@domain = @mailuser.domain
  end

  # POST /mailusers
  # POST /mailusers.xml
  def create
    @mailuser = Mailuser.new(params[:mailuser])
		@domain = @mailuser.domain
		allowed_to_edit(@mailuser)
		if params[:password][:one] != ""
			if params[:password][:one] == params[:password][:two]
				@mailuser.setpassword(params[:password][:one])
				saved = @mailuser.save
			else
				saved = false
			end
		else
			saved = false
		end
		
    respond_to do |format|
      if saved
        flash[:notice] = 'Mailusers was successfully created.'
        format.html { redirect_to(@mailuser) }
        format.xml  { render :xml => @mailuser, :status => :created, :location => @mailuser }
      else
        flash[:notice] = 'Unable to create user. Passwords were not the same'
        format.html { render :action => "new" }
        format.xml  { render :xml => @mailuser.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /mailusers/1
  # PUT /mailusers/1.xml
  def update
    @mailuser = Mailuser.find(params[:id])
		allowed_to_edit(@mailuser)

		if params[:password][:one] != ""
			if params[:password][:one] == params[:password][:two]
				@mailuser.setpassword(params[:password][:one])
				saved = @mailuser.update_attributes(params[:mailuser])
			else
				saved = false
			end
		else
			saved = @mailuser.update_attributes(params[:mailuser])
		end		
		
    respond_to do |format|
      if saved
        flash[:notice] = 'Mailusers was successfully updated.'
        format.html { redirect_to(@mailuser) }
        format.xml  { head :ok }
      else
        flash[:notice] = 'Unable to save user.'
        format.html { render :action => "edit" }
        format.xml  { render :xml => @mailuser.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /mailusers/1
  # DELETE /mailusers/1.xml
  def destroy
    @mailuser = Mailuser.find(params[:id])
		@domain = @mailuser.domain
		if @currentuser.administrator or @currentuser == @domain.owner 
    	@mailuser.destroy
		end

    respond_to do |format|
			format.html { redirect_to(:action => "show", :controller => "domains", :id => @domain.id) }
      format.xml  { head :ok }
    end
  end
end
