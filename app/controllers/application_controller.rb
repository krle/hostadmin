class ApplicationController < ActionController::Base
  helper :all # include all helpers, all the time
	layout 'base'
  protect_from_forgery
  
	def logged_in
		if session["login"] != nil and session["password"] != nil
			user = session["login"]
			passwordhash = session["password"]
			@currentuser = User.authenticate(user, passwordhash,
				{ :pwdhashed => true })
		else 
			authenticate_with_http_basic do |username, password|
				@currentuser = User.authenticate(username, password, { :pwdhashed => false} )
		  end
		end
	
		if @currentuser == nil
			redirect_to :controller => 'index', :action => 'login'
		end
	end

	def logged_in_administrator
		logged_in

		if @currentuser != nil
			if !@currentuser.administrator
				redirect_to :controller => 'index', :action => 'login'
			end
		end
	end
end
