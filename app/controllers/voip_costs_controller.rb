class VoipCostsController < ApplicationController
  before_filter :logged_in
  before_filter :logged_in_administrator, :except => ["show"]
  # GET /voip_costs
  # GET /voip_costs.json
  def index
    @voip_costs = VoipCost.order("start_date desc").all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @voip_costs }
    end
  end

  # GET /voip_costs/1
  # GET /voip_costs/1.json
  def show
    @voip_cost = VoipCost.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @voip_cost }
    end
  end

  # GET /voip_costs/new
  # GET /voip_costs/new.json
  def new
    @voip_cost = VoipCost.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @voip_cost }
    end
  end

  def duplicate
    voip_cost = VoipCost.find(params[:id])
    @voip_cost = voip_cost.dup
    @voip_cost.start_date = Date.today
    render :action => "new"
  end

  # GET /voip_costs/1/edit
  def edit
    @voip_cost = VoipCost.find(params[:id])
  end

  # POST /voip_costs
  # POST /voip_costs.json
  def create
    @voip_cost = VoipCost.new(params[:voip_cost])

    respond_to do |format|
      if @voip_cost.save
        format.html { redirect_to @voip_cost, notice: 'Voip cost was successfully created.' }
        format.json { render json: @voip_cost, status: :created, location: @voip_cost }
      else
        format.html { render action: "new" }
        format.json { render json: @voip_cost.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /voip_costs/1
  # PUT /voip_costs/1.json
  def update
    @voip_cost = VoipCost.find(params[:id])

    respond_to do |format|
      if @voip_cost.update_attributes(params[:voip_cost])
        format.html { redirect_to @voip_cost, notice: 'Voip cost was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @voip_cost.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /voip_costs/1
  # DELETE /voip_costs/1.json
  def destroy
    @voip_cost = VoipCost.find(params[:id])
    @voip_cost.destroy

    respond_to do |format|
      format.html { redirect_to voip_costs_url }
      format.json { head :ok }
    end
  end
end
