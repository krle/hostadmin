class MailaliasSourcesController < ApplicationController
	before_filter :logged_in

	def allowed_to_edit(mailuser)
		if mailuser.domain.owner != @currentuser and !@currentuser.administrator
			redirect_to(:action => "index", :controller => "domains")
		end
	end

  def show
    @mailalias_source = MailaliasSource.find(params[:id])
		allowed_to_edit(@mailalias_source)
		@domain = @mailalias_source.domain
		@new_destination = MailaliasDestination.new
		@new_destination.source = @mailalias_source

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @mailalias_source }
    end
  end

  def new
    @mailalias_source = MailaliasSource.new
		@domain = Domain.find(params[:domain_id])

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @mailuser }
    end
  end

  def edit
    @mailalias_source = MailaliasSource.find(params[:id])
		allowed_to_edit(@mailalias_source)
		@domain = @mailalias_source.domain
		@new_destination = MailaliasDestination.new
		@new_destination.source = @mailalias_source
  end

  def create
    @mailalias_source = MailaliasSource.new(params[:mailalias_source])
		@domain = Domain.find(params[:domain_id])
		@mailalias_source.domain = @domain
		allowed_to_edit(@mailalias_source)
		saved = @mailalias_source.save

    respond_to do |format|
      if saved
        flash[:notice] = 'Mailalias Source was successfully created.'
        format.html { redirect_to(@mailalias_source) }
        format.xml  { render :xml => @mailalias_source, :status => :created, :location => @mailuser }
      else
        flash[:notice] = 'Unable to create alias.'
        format.html { render :action => "new" }
        format.xml  { render :xml => @mailalias_source.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    @mailalias_source = MailaliasSource.find(params[:id])
		allowed_to_edit(@mailalias_source)
		saved = @mailalias_source.update_attributes(params[:mailalias_source])

    respond_to do |format|
      if saved
        flash[:notice] = 'Mailalias Source was successfully created.'
        format.html { redirect_to(@mailalias_source) }
        format.xml  { head :ok }
      else
        flash[:notice] = 'Unable to save alias.'
        format.html { render :action => "edit" }
        format.xml  { render :xml => @mailalias_source.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @mailalias_source = MailaliasSource.find(params[:id])
		@domain = @mailalias_source.domain
		if @currentuser.administrator or @currentuser == @domain.owner 
    	@mailalias_source.destroy
		end

    respond_to do |format|
			format.html { redirect_to(:action => "show", :controller => "domains", :id => @domain.id) }
      format.xml  { head :ok }
    end
  end
end