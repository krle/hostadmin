class CreateMailusers < ActiveRecord::Migration
  def self.up
    create_table :mailusers do |t|
			t.references :domain, :null => false
			t.string :name, :null => false
			t.string :password, :null => false
			t.integer :quota
      t.timestamps
    end
		add_index(:mailusers, :name)
		add_index(:mailusers, :domain_id)
  end

  def self.down
    drop_table :mailusers
  end
end
