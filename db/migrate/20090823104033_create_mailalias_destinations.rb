class CreateMailaliasDestinations < ActiveRecord::Migration
  def self.up
    create_table :mailalias_destinations do |t|
			t.references :mailalias_source, :null => false
			t.string :destination, :null => false
      t.timestamps
    end
		add_index(:mailalias_destinations, :mailalias_source_id)
		
		forwards = Mailforward.find(:all, :order => "domain_id, source")
		if (forwards.count > 0)
			for forward in forwards
				source = MailaliasSource.find(:first, :conditions => ["source = ? AND domain_id = ?", forward.source, forward.domain_id])
				destination = MailaliasDestination.new
				destination.destination = forward.destination
				destination.source = source
				destination.save
			end
		end
  end

  def self.down
    drop_table :mailalias_destinations
  end
end
