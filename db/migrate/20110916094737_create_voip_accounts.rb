class CreateVoipAccounts < ActiveRecord::Migration
  def change
    create_table :voip_accounts do |t|
    	t.references :owner, :null => false
    	t.string :name
    	t.string :password
    	t.integer :channels
    	t.timestamps
    end
  end
end
