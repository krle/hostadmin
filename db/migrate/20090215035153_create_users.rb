class CreateUsers < ActiveRecord::Migration
  def self.up
    create_table :users do |t|
			t.string :login, :null => false
			t.string :name, :null => false
			t.string :address
			t.string :email
			t.string :password, :null => false
			t.boolean :administrator, :null => false, :defalut => false
      t.timestamps
    end

		lordmortis = User.new
		lordmortis.login = "lordmortis"
		lordmortis.name = "Brendan Ragan"
		lordmortis.email = "lordmortis@gmail.com"
		lordmortis.setpassword("temp")
		lordmortis.administrator = true
		lordmortis.save
  end

  def self.down
    drop_table :users
  end
end
