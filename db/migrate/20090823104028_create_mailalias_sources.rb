class CreateMailaliasSources < ActiveRecord::Migration
  def self.up
    create_table :mailalias_sources do |t|
			t.references :domain, :null => false
			t.string :source, :null => false
      t.timestamps
    end
		add_index(:mailalias_sources, :source)
		add_index(:mailalias_sources, :domain_id)

		forwards = Mailforward.find(:all, :order => "domain_id, source")
		if (forwards.count > 0)
			source = MailaliasSource.new
			source.domain = forwards[0].domain
			source.source = forwards[0].source
			source.save
			for forward in forwards
				if (source.source != forward.source) or (source.domain != forward.domain)
					source = MailaliasSource.new
					source.domain = forward.domain
					source.source = forward.source
					source.save
				end
			end
		end
  end

  def self.down
    drop_table :mailalias_sources
  end
end
