class AddEmailFlag < ActiveRecord::Migration
  def self.up
		add_column :domains, :email, :boolean, :null => false, :default => false
  end

  def self.down
		remove_column :domains, :email
  end
end
