class CreateVoipCosts < ActiveRecord::Migration
  def change
    create_table :voip_costs do |t|
      t.string :number
      t.string :name
      t.integer :flagfall, :default => 0
      t.float :timecost, :default => 0
      t.integer :timeperiod, :default => 1
      t.timestamp :start_date

      t.timestamps
    end

    mobiles = VoipCost.new(:number => "+614________", :name => "Australian Mobile", :timecost => 25, :timeperiod => 60, :start_date => "2008-01-01 00:01")
    mobiles.save

    landlines = VoipCost.new(:number => "+61_________", :name => "Australian Land Line", :flagfall => 10, :start_date => "2008-01-01 00:01")
    landlines.save

    onethree = VoipCost.new(:number => "+6113%", :name => "Australian 13 Number", :flagfall => 25, :start_date => "2008-01-01 00:01")
    onethree.save

    onethree2 = VoipCost.new(:number => "+6113%", :name => "Australian 13 Number", :flagfall => 30, :start_date => "2010-01-01 00:01")
    onethree2.save    

    oneeight = VoipCost.new(:number => "+6118%", :name => "Australian 18 Number", :flagfall => 25, :start_date => "2008-01-01 00:01")
    oneeight.save

    cost = VoipCost.new(:number => "+1780%", :name => "Canada - North Alberta", :timecost => 7, :timeperiod => 60, :start_date => "2008-01-01 00:01")
    cost.save

    cost = VoipCost.new(:number => "+1403%", :name => "Canada - South Alberta", :timecost => 7, :timeperiod => 60, :start_date => "2008-01-01 00:01")
    cost.save

    cost = VoipCost.new(:number => "+1250%", :name => "Canada - British Columbia", :timecost => 7, :timeperiod => 60, :start_date => "2008-01-01 00:01")
    cost.save

    cost = VoipCost.new(:number => "+1604%", :name => "Canada - Vancouver", :timecost => 7, :timeperiod => 60, :start_date => "2008-01-01 00:01")
    cost.save

    cost = VoipCost.new(:number => "+1807%", :name => "Canada - Fort William", :timecost => 7, :timeperiod => 60, :start_date => "2008-01-01 00:01")
    cost.save

    cost = VoipCost.new(:number => "+57%", :name => "Colombia - Land Line", :timecost => 20, :timeperiod => 60, :start_date => "2008-01-01 00:01")
    cost.save

    cost = VoipCost.new(:number => "+573%", :name => "Colombia - Mobile", :timecost => 24, :timeperiod => 60, :start_date => "2008-01-01 00:01")
    cost.save

    cost = VoipCost.new(:number => "+8180%", :name => "Japan - Mobile", :timecost => 40, :timeperiod => 60, :start_date => "2008-01-01 00:01")
    cost.save
  end
end
