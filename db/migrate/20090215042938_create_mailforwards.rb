class CreateMailforwards < ActiveRecord::Migration
  def self.up
    create_table :mailforwards do |t|
			t.references :domain, :null => false
			t.string :source, :null => false
			t.string :destination, :null => false
      t.timestamps
    end
		add_index(:mailforwards, :source)
		add_index(:mailforwards, :domain_id)		
  end

  def self.down
    drop_table :mailforwards
  end
end
