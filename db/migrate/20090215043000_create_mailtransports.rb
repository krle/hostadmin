class CreateMailtransports < ActiveRecord::Migration
  def self.up
    create_table :mailtransports do |t|
			t.references :domain, :null => false
			t.string :transport, :null => false
      t.timestamps
    end
		add_index(:mailtransports, :domain_id)
  end

  def self.down
    drop_table :mailtransports
  end
end
