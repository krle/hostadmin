# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20110916114141) do

  create_table "domains", :force => true do |t|
    t.string   "name"
    t.date     "paiduntil"
    t.integer  "owner_id",                      :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "email",      :default => false, :null => false
  end

  add_index "domains", ["name"], :name => "index_domains_on_name"

  create_table "mailalias_destinations", :force => true do |t|
    t.integer  "mailalias_source_id", :null => false
    t.string   "destination",         :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "mailalias_destinations", ["mailalias_source_id"], :name => "index_mailalias_destinations_on_mailalias_source_id"

  create_table "mailalias_sources", :force => true do |t|
    t.integer  "domain_id",  :null => false
    t.string   "source",     :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "mailalias_sources", ["domain_id"], :name => "index_mailalias_sources_on_domain_id"
  add_index "mailalias_sources", ["source"], :name => "index_mailalias_sources_on_source"

  create_table "mailforwards", :force => true do |t|
    t.integer  "domain_id",   :null => false
    t.string   "source",      :null => false
    t.string   "destination", :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "mailforwards", ["domain_id"], :name => "index_mailforwards_on_domain_id"
  add_index "mailforwards", ["source"], :name => "index_mailforwards_on_source"

  create_table "mailtransports", :force => true do |t|
    t.integer  "domain_id",  :null => false
    t.string   "transport",  :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "mailtransports", ["domain_id"], :name => "index_mailtransports_on_domain_id"

  create_table "mailusers", :force => true do |t|
    t.integer  "domain_id",  :null => false
    t.string   "name",       :null => false
    t.string   "password",   :null => false
    t.integer  "quota"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "mailusers", ["domain_id"], :name => "index_mailusers_on_domain_id"
  add_index "mailusers", ["name"], :name => "index_mailusers_on_name"

  create_table "users", :force => true do |t|
    t.string   "login",         :null => false
    t.string   "name",          :null => false
    t.string   "address"
    t.string   "email"
    t.string   "password",      :null => false
    t.boolean  "administrator", :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "voip_accounts", :force => true do |t|
    t.integer  "owner_id",   :null => false
    t.string   "name"
    t.string   "password"
    t.integer  "channels"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "voip_costs", :force => true do |t|
    t.string   "number"
    t.string   "name"
    t.integer  "flagfall",   :default => 0
    t.float    "timecost",   :default => 0.0
    t.integer  "timeperiod", :default => 1
    t.datetime "start_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
