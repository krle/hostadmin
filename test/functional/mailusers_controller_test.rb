require 'test_helper'

class MailusersControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mailusers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mailusers" do
    assert_difference('Mailusers.count') do
      post :create, :mailusers => { }
    end

    assert_redirected_to mailusers_path(assigns(:mailusers))
  end

  test "should show mailusers" do
    get :show, :id => mailusers(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => mailusers(:one).id
    assert_response :success
  end

  test "should update mailusers" do
    put :update, :id => mailusers(:one).id, :mailusers => { }
    assert_redirected_to mailusers_path(assigns(:mailusers))
  end

  test "should destroy mailusers" do
    assert_difference('Mailusers.count', -1) do
      delete :destroy, :id => mailusers(:one).id
    end

    assert_redirected_to mailusers_path
  end
end
