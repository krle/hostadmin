require 'test_helper'

class VoipAccountsControllerTest < ActionController::TestCase
  setup do
    @voip_account = voip_accounts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:voip_accounts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create voip_account" do
    assert_difference('VoipAccount.count') do
      post :create, voip_account: @voip_account.attributes
    end

    assert_redirected_to voip_account_path(assigns(:voip_account))
  end

  test "should show voip_account" do
    get :show, id: @voip_account.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @voip_account.to_param
    assert_response :success
  end

  test "should update voip_account" do
    put :update, id: @voip_account.to_param, voip_account: @voip_account.attributes
    assert_redirected_to voip_account_path(assigns(:voip_account))
  end

  test "should destroy voip_account" do
    assert_difference('VoipAccount.count', -1) do
      delete :destroy, id: @voip_account.to_param
    end

    assert_redirected_to voip_accounts_path
  end
end
