require 'test_helper'

class VoipCostsControllerTest < ActionController::TestCase
  setup do
    @voip_cost = voip_costs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:voip_costs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create voip_cost" do
    assert_difference('VoipCost.count') do
      post :create, voip_cost: @voip_cost.attributes
    end

    assert_redirected_to voip_cost_path(assigns(:voip_cost))
  end

  test "should show voip_cost" do
    get :show, id: @voip_cost.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @voip_cost.to_param
    assert_response :success
  end

  test "should update voip_cost" do
    put :update, id: @voip_cost.to_param, voip_cost: @voip_cost.attributes
    assert_redirected_to voip_cost_path(assigns(:voip_cost))
  end

  test "should destroy voip_cost" do
    assert_difference('VoipCost.count', -1) do
      delete :destroy, id: @voip_cost.to_param
    end

    assert_redirected_to voip_costs_path
  end
end
